# frozen_string_literal: true

class Courier < ApplicationRecord
  # has_many :packages
  has_many :packages
end
